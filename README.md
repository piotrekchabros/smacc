# README #

This README would normally document whatever steps are necessary to get your application up and running.

### SMACC SOLUTION - SMALL EMAIL SENDING APPLICATION DEMO/PROTOTYPE ###

The purpose of this application is to provide a simple interface and backend implementation for various email server providers.
This application is just a prototype - it is missing some functionality. It is just a demo/prototype.

### How to set it up? ###

The application is easily deployable to external web application containers e.g. Tomcat
It is also possible to quickly run the application using an embedded server (development purposes)
It is possible to provide specific application configuration (e.g. email providers) by using environmental variables and properties file

After launch, the application serves the main UX at http://localhost:8080

## Architecture: how clean is the separation between the front-end and the back-end? ##

The application is clearly divided into different segments. It is following the Model-View-Controler convention.

## Is anything missing? #

The app is production ready in terms of fast and easy deployment as well as possible flexible configuration of application properties.

This application would consume additional time and effort in order to extend. Possible ways of improvements could consist of preparing and creating suitable interfaces and classes to provide validation, logging, better code separation, better logic, etx.

## Code contents ##

![code structure](https://i.snag.gy/DwFb6s.jpg)

The code is divided into several packages:

| package name              	| description    																|
| -------------             	| ------------- 																|
| com.smacc.solution.config 	| consists of the core application configuration classes						|
| com.smacc.solution.controller | holds the main class responsible for handling HTTP requests and doing logic	|
| com.smacc.solution.message | just a single class for keeping the email message data|
| com.smacc.solution.email | abstract layer of access to various implementations of different email providers|
| webapp | it contains some environmental properties (application.properties), static resources like js,images,css, and also the main .html files (views/templates)|
| test | package is ment covering the application with unit and integration tests |

## Abstract email server provider layer and failover implementation ##

The abstract layer for various email providers was implemented with an abstract EmailProvider class. The abstract class contains the necessary functions and fields common for all email provider implementations.
The failover implementation is not sophisticated at this stage as the application is containing only 2 email provider implementations. This failover takes place in the main controller function, by checking for any errors in the response code and catching possible exceptions.
This implementation is not complete because as we value quality over completeness, and the implementation is time consuming, I decided to rather well document my ideas instead of completely implementing all of them.

## Security ##

In order to make this application more secure the should be a simple user authentication and authorization mechanism implemented, at least CAPTCHA protection. 

## Testing ##

In the email server provider's application console the total count of emails sent and their parameters, details and errors are visible:

![Email sent test](https://i.snag.gy/nMjzbR.jpg)

## UX ##

The interface is very simple and straightforward. The interface consists of a single form with fields such as:
- from address
- to address 
- subject
- message (text) 

The front end is using the popular bootstrap.css implementation to keep professional and eye-catching looks.

![UX](https://i.snag.gy/XSVIaF.jpg)

Bootstrap provided the form with the basic validation functionality of the email input.

Possible extension could consist of making the form send the data asynchronously, using AJAX, and display appropriate messages (auccess/warning/info/error). 


### Author ###

* Piotr Chabros