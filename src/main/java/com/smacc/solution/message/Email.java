package com.smacc.solution.message;

public class Email {

    private String senderEmail;
    private String recipientEmail;
    private String subject;
    private String text;

    public Email(){

    }

    public void setSenderEmail(String email) {
        this.senderEmail = email;
    }

    public void setRecipientEmail(String email)
    {
        this.recipientEmail = email;
    }

    public String getSenderEmail(){
        return senderEmail;
    }

    public String getRecipientEmail(){
        return recipientEmail;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
