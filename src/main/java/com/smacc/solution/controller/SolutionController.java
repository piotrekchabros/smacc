package com.smacc.solution.controller;

import com.smacc.solution.email.Mailgun;
import com.smacc.solution.email.SendGridProvider;
import com.smacc.solution.email.provider.EmailProvider;
import com.smacc.solution.message.Email;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class SolutionController {

    @RequestMapping("/")
    public ModelAndView getIndex(@ModelAttribute Email email) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");

        if(email.getSenderEmail() != null) //proceed if the email is provided
        {
            Mailgun emailProvider = new Mailgun(); //use mailgun
            sendEmail(modelAndView, emailProvider, email);
        }

        return modelAndView;
    }

    public void sendEmail(ModelAndView modelAndView, EmailProvider emailProvider, Email email)
    {
        int statusCode;
        try {
            statusCode = emailProvider.send(email);
            //implement validation accordingly to the status code
        }
        catch (Exception e) { //in case of fail try again with sendgrid
            if(Mailgun.class.isInstance(emailProvider))
            {
                sendEmail(modelAndView, new SendGridProvider(), email);
            }
            else
            {
                modelAndView.addObject("status", "error");
            }
        }
        modelAndView.addObject("status", "success");
    }
}
