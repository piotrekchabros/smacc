package com.smacc.solution.email;

import com.smacc.solution.email.provider.EmailProvider;
import com.smacc.solution.message.Email;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.springframework.beans.factory.annotation.Value;

import javax.ws.rs.core.MediaType;

public class Mailgun extends EmailProvider {

    @Value("${api.key.mailgun}")
    private static String API_KEY;

    @Value("${api.url.mailgun}")
    private static String API_URL;

    public Mailgun()
    {

    }

    public static ClientResponse SendSimpleMessage(String from, String to, String subject, String text) {
        Client client = Client.create();
        client.addFilter(new HTTPBasicAuthFilter("api", API_KEY));
        WebResource webResource = client.resource(API_URL);
        MultivaluedMapImpl formData = new MultivaluedMapImpl();
        formData.add("from", from);
        formData.add("to", to);
        formData.add("subject", subject);
        formData.add("text", text);
        return webResource.type(MediaType.APPLICATION_FORM_URLENCODED).
                post(ClientResponse.class, formData);
    }

    public int send(Email email)
    {
        return SendSimpleMessage(
                email.getSenderEmail(),
                email.getRecipientEmail(),
                email.getSubject(),
                email.getText()
        ).getStatus();
    }
}
