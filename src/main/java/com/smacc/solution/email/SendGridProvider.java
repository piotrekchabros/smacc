package com.smacc.solution.email;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.smacc.solution.email.provider.EmailProvider;
import com.smacc.solution.message.Email;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;

public class SendGridProvider extends EmailProvider {

    @Value("${api.key.sendgrid}")
    public static String API_KEY;

    @Value("${api.url.sendgrid")
    public static String API_URL;


    public int send(Email email) throws IOException
    {
        try
        {
            SendGrid sg = new SendGrid(API_KEY);
            Request request = new Request();
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody("{\"personalizations\":[{\"to\":[{\"email\":\""
                    + email.getRecipientEmail() + "\"}],\"subject\":\""
                    + email.getSubject() + "\"}],\"from\":{\"email\":\""
                    + email.getSenderEmail() + "\"},\"content\":[{\"type\":\"text/plain\",\"value\": \""
                    + email.getText() + "\"}]}");
            Response response = sg.api(request);
            return response.getStatusCode();
        }
        catch (IOException ex)
        {
            throw ex;
        }
    }

}
