package com.smacc.solution.email.provider;

import com.smacc.solution.message.Email;

import java.io.IOException;

public abstract class EmailProvider {

    public static String API_KEY;
    public static String API_URL;

    public int send(Email email) throws IOException, Exception {
        throw new Exception("Not implemented!");
    }

}
